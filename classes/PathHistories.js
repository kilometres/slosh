var PathHistory = require('./PathHistory');

class PathHistories {
    constructor() {
        this.pathHistories = new Map();
    }
    newVersion(path) {
        var history = this.pathHistories.get(path.id);
        if(!history) {
            pathHistory = new PathHistory(path);
            this.pathHistorie.set(path.id, pathHistory);
        } else {
            history.newVersion(path);
            this.pathHistories.set(path.id, history);
        }
    }
    undo(id) {
        var pathHistory = this.pathHistories.get(id);
        pathHistory.undo();
        this.pathHistories.set(id, pathHistory);
    }
    redo(id) {
        var pathHistory = this.pathHistories.get(id);
        pathHistory.redo();
        this.pathHistories.set(id, pathHistory);
    }
    current(id) {
        return this.pathHistories.get(id).current();
    }
}

module.exports = PathHistories;
