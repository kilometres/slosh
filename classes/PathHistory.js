class PathHistory{
    constructor(path) {
        this.place = 0;
        this.history = [path];
    }
    newVersion(path) {
        if(this.history.length > 0) {
            this.place += 1;
            this.history.splice(this.place, this.history.length - 1 - this.place, path);
        }
    }
    undo() {
        this.place = this.place - 1 >= 0 ? this.place - 1 : 0;
        return this.current();
    }
    redo() {
        this.place = this.place + 1 < this.history.length ? this.place + 1 : this.place;
        return this.current();
    }
    current() {
        return this.history[this.place];
    }
}

module.exports = PathHistory;
