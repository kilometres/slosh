'use strict';

var Fluxible = require('fluxible');

// create new fluxible instance
var app = new Fluxible({
    component: require('./components/Routes.jsx')
});

// register stores
app.registerStore(require('./stores/SettingsStore'));
app.registerStore(require('./stores/PathStore'));
app.registerStore(require('./stores/DeckStore'));
app.registerStore(require('./stores/GameStore'));
app.registerStore(require('./stores/SimulationStore'));
app.registerStore(require('./stores/UndoStore'));

module.exports = app;
