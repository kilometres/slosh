'use strict';
var restful = require('node-restful');
var mongoose = restful.mongoose;
var Schema = mongoose.Schema;

module.exports = restful.model('Step', Schema({
    instructions: {type: String, default: 'Drink.'},
    type: {type: Object, default: {id: 0, name: 'single'}},
    round: {type: Number, default: 1}
  }))
  .methods(['get', 'post', 'put', 'delete']);
