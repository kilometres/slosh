'use strict';
var restful = require('node-restful');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = mongoose.model('Path', Schema({
    name: String,
    description: String,
    number: Number,
    maximumUses: Number,
    steps: [{
        instructions: {type: String, default: 'New instructions'},
        kind: {
            id: {type: Number, default: 0},
            name: {type: String, default: 'single'}
        },
        round: {type: Number, default: 1},
        video: Boolean,
        weight: Number
    }],
  }));
