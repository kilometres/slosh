'use strict';
var React = require('react');

var AddStep = React.createClass({
    getInitialState: function () {
        return this.getState();
    },
    getState: function () {
        return {

        };
    },
    render: function() {
        return (
            <div className="add-step" onClick={this.props.action}>
              +
            </div>
        );
    }
});

module.exports = AddStep;
