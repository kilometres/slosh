'use strict';
var React = require('react');

var EditableInput = React.createClass({
    getInitialState: function () {
        return {
            editing: this.props.editing || false,
            text: this.props.defaultValue
        }
    },
    render: function() {
        var html;
        if(!this.state.editing)
            html = (
                <span>{this.state.text}</span>
            );
        else
            html = (
                <input type={this.props.type || "text"} defaultValue={this.state.text} ref="input" onBlur={this.stopEditing} min={this.props.min || 1} max={this.props.max || 99} size={this.props.size}/>
            );

        var label = this.props.label ? ( <label for={this.props.id}>{this.props.label}</label> ) : "";
        return (
            <div className={"editable " + this.props.className} onClick={this.startEditing}>
                {label}
                {html}
            </div>
        );
    },
    startEditing: function() {
        if(!this.state.editing) {
            this.setState({
                editing: true
            });
            setTimeout(function() {
                this.refs.input.getDOMNode().focus();
                this.refs.input.getDOMNode().select();
            }.bind(this), 20)
        }
    },
    stopEditing: function() {
      var value = event.target.value;
      if(this.props.valueRequired && value == ""){
          value = this.state.text;
      }
      this.setState({
          editing: false,
          text: value
      });
      if(this.props.onFieldChange)
          this.props.onFieldChange(this.props.id, value);
    }
});

module.exports = EditableInput;
