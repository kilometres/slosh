'use strict';
var React = require('react');
var EditableInput = require('./EditableInput');
var SimulationResults = require('./SimulationResults');
var FluxibleMixin = require('fluxible/lib/FluxibleMixin');
var SimulationStore = require('../stores/SimulationStore');
var simulate = require('../actions/simulate');
var updateSetting = require('../actions/updateSetting');

var Simulation = React.createClass({
    mixins: [FluxibleMixin],
    statics: {
        storeListeners: [SimulationStore]
    },
    getInitialState: function () {
        return this.getState();
    },
    getState: function () {
        var simulationStore = this.getStore(SimulationStore);
        return {
          results: simulationStore.getResults()
        };
    },
    onChange: function () {
        var state = this.getState();
        this.setState(state);
    },
    simulate: function () {
        this.context.executeAction(simulate);
    },
    render: function() {
        return (
            <div className="simulation">
                <EditableInput id="rounds" className="" type="number" defaultValue={5} valueRequired={true} label="rounds" onFieldChange={this.updateSetting}/>
                <EditableInput id="cycles" className="" type="number" defaultValue={1} valueRequired={true} label="cycles" onFieldChange={this.updateSetting}/>
                <button onClick={this.simulate}>Simulate</button>
                <SimulationResults results={this.state.results}/>
            </div>
        );
    },
    updateSetting: function(field, value) {
        this.context.executeAction(updateSetting, {field: field, value: value});
    }
});

module.exports = Simulation;
