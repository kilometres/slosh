var React = require('react');
var Route = require('react-router').Route;
var DefaultRoute = require('react-router').DefaultRoute;
var Application = require('./Application.jsx');
var Paths = require('./Paths.jsx');
var EditPath = require('./EditPath.jsx');
var NewPath = require('./NewPath.jsx');
var Simulation = require('./Simulation');

var routes = (
    <Route name="app" path="/" handler={Application}>
        <Route name="path" handler={EditPath} path="path/:id"/>
        <Route name="newPath" handler={NewPath} path="new/path"/>
        <Route name="simulation" handler={Simulation} path="simulate"/>
        <DefaultRoute name="dash" handler={Paths}/>
    </Route>
);

module.exports = routes;
