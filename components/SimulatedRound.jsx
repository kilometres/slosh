'use strict';
var React = require('react');
var SimulatedTurn = require('./SimulatedTurn');

var SimulatedRound = React.createClass({
    render: function() {
        var turnsDom = [];
        this.props.round.forEach(function(turn, index) {
            turnsDom.push(
                <SimulatedTurn key={index} turn={turn}/>
            );
        })
        return (
            <div className="simulated-round">
                <h2>{this.props.roundNumber}</h2>
                {turnsDom}
            </div>
        );
    },

});

module.exports = SimulatedRound;
