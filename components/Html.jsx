/**
 * Copyright 2014, Yahoo! Inc.
 * Copyrights licensed under the New BSD License. See the accompanying LICENSE file for terms.
 */
'use strict';
var React = require('react');

/**
 * React class to handle the rendering of the HTML head section
 *
 * @class Head
 * @constructor
 */
var Html = React.createClass({
    /**
     * Refer to React documentation render
     *
     * @method render
     * @return {Object} HTML head section
     */
    render: function() {
        return (
            <html>
            <head>
                <meta charSet="utf-8" />
                <title>{this.props.title}</title>
                <meta name="viewport" content="width=device-width, user-scalable=no" />
                <link href='http://fonts.googleapis.com/css?family=Lato:300,400|Slabo+27px' rel='stylesheet' type='text/css'/>
                <link rel="stylesheet" href="/bower/bootstrap/dist/css/bootstrap.min.css" />
                <link rel="stylesheet" href="/public/stylesheets/main.css" />
            </head>
            <body>
                <div id="app" dangerouslySetInnerHTML={{__html: this.props.markup}}></div>
            </body>
            <script dangerouslySetInnerHTML={{__html: this.props.state}}></script>
            <script src="/bower/jquery/dist/jquery.min.js"></script>
            <script src="/bower/bootstrap/dist/js/bootstrap.min.js"></script>
            <script src="/public/js/main.js" defer></script>
            </html>
        );
    }
});

module.exports = Html;
