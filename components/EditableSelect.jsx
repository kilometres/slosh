'use strict';
var React = require('react');

var EditableSelect = React.createClass({
    getInitialState: function () {
        return {
            editing: this.props.editing || false,
            selected: this.props.selected
        }
    },
    render: function() {
        var html;
        var options = this.props.options;
        var optionsDOM = [];
        options.forEach(function(option, index) {
            optionsDOM.push(
                <option value={option.id} key={index}>
                    {option.name}
                </option>
            );
        });
        if(!this.state.editing)
            html = (
                <span>{this.state.selected.name}</span>
            );
        else
            html = (
                <select defaultValue={this.state.selected.id} ref="select" onBlur={this.stopEditing}>
                    {optionsDOM}
                </select>
            );

        var label = this.props.label ? ( <label for={this.props.id}>{this.props.label}</label> ) : "";
        return (
            <div className="editable" onClick={this.startEditing}>
                {label}
                {html}
            </div>
        );
    },
    startEditing: function() {
        this.setState({
            editing: true
        });
        setTimeout(function() {
            this.refs.select.getDOMNode().focus();
        }.bind(this), 20)
    },
    stopEditing: function() {
      var value = event.target.value;
      // if(this.props.valueRequired && value == ""){
      //     value = this.state.selected.id;
      // }
      var selected = this.state.selected;
      this.props.options.forEach(function(option){
          if(option.id == value)
              selected = option;
      });
      this.setState({
          editing: false,
          selected: selected
      });
      if(this.props.onFieldChange)
          this.props.onFieldChange(this.props.id, selected);
    }
});

module.exports = EditableSelect;
