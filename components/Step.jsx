'use strict';
var React = require('react');
var EditableInput = require('./EditableInput.jsx');
var EditableSelect = require('./EditableSelect');
var EditableToggle = require('./EditableToggle');

var Path = React.createClass({
    getInitialState: function () {
        return this.getState();
    },
    getState: function () {
        return {

        };
    },
    render: function() {
        var step = this.props.step
        if(step)
            return (
                <div className="step">
                    <button className="button float-right button-up" onClick={this.decrease}><span className="glyphicon glyphicon-chevron-up"/></button>
                    <EditableInput id="instructions" className="center" type="text" defaultValue={step.instructions} valueRequired={true} onFieldChange={this.onFieldChange}/>
                    <hr/>
                    <EditableSelect id="kind" className="" selected={step.kind} options={this.props.settings.stepTypes} valueRequired={true} label="kind" onFieldChange={this.onFieldChange}/>
                    <EditableInput id="round" className="" type="number" defaultValue={step.round} size={3} valueRequired={true} label="round" onFieldChange={this.onFieldChange}/>
                    <EditableInput id="weight" className="" type="number" defaultValue={step.weight} size={3} valueRequired={true} label="weight" onFieldChange={this.onFieldChange}/>
                    <EditableToggle id="video" className="" defaultValue={step.video} label="video" onFieldChange={this.onFieldChange}/>
                    <button className="button float-righ button-down" onClick={this.increase}><span className="glyphicon glyphicon-chevron-down"/></button>
                </div>
            );
        else return (<span>Loading...</span>);
    },
    onFieldChange: function(field, value) {
        var step = this.props.step;
        step[field] = value;
        this.props.onStepChange(step, this.props.index);
    },
    decrease: function() {
        this.props.moveStep(this.props.index, this.props.index - 1)
    },
    increase: function() {
        this.props.moveStep(this.props.index, this.props.index + 1)
    }
});

module.exports = Path;
