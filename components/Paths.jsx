'use strict';
var React = require('react');
var PathSummary = require('./PathSummary.jsx');
var deletePath = require('../actions/deletePath.js');
var FluxibleMixin = require('fluxible/lib/FluxibleMixin');
var Navigation = require('react-router').Navigation;

var Paths = React.createClass({
    mixins: [FluxibleMixin, Navigation],
    getInitialState: function () {
        return this.getState();
    },
    getState: function () {
        return {

        };
    },
    render: function() {
        var paths = this.props.paths;
        var pathsDOM = [];
        paths.forEach(function(path) {
            pathsDOM.push(
                <div className="col-md-12" key={path._id}>
                    <PathSummary key={path._id} path={path}/>
                    <div onClick={function(){this.delete(path._id);}.bind(this)}><span className="glyphicon glyphicon-remove"/></div>
                </div>
            );
        }.bind(this));
        return (
            <div className="row">
                {pathsDOM.map(function(path) {
                  return path;
                })}
            </div>
        );
    },
    delete: function(id) {
        this.context.executeAction(deletePath, { id: id, caller: this });
    }
});

module.exports = Paths;
