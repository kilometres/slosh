'use strict';
var React = require('react');

var EditableTextArea = React.createClass({
    getInitialState: function () {
        return {
            editing: this.props.editing || false,
            text: this.props.defaultValue
        }
    },
    render: function() {
        var html;
        if(!this.state.editing)
            html = (
                <p>{this.state.text}</p>
            );
        else
            html = (
                <textarea rows={this.props.rows || 2} cols={this.props.cols || 40} defaultValue={this.state.text} ref="input" onBlur={this.stopEditing}/>
            );

        var label = this.props.label ? ( <label for={this.props.id}>{this.props.label}</label> ) : "";
        return (
            <div className="editable" onClick={this.startEditing}>
                {label}
                {html}
            </div>
        );
    },
    startEditing: function() {
        this.setState({
            editing: true
        });
        setTimeout(function() {
            this.refs.input.getDOMNode().focus();
        }.bind(this), 20)
    },
    stopEditing: function() {
      var value = event.target.value;
      if(this.props.valueRequired && value == ""){
          value = this.props.defaultText;
      }
      this.setState({
          editing: false,
          text: value
      });
    }
});

module.exports = EditableTextArea;
