'use strict';
var React = require('react');
var Step = require('./Step.jsx');

var StepList = React.createClass({
    getInitialState: function () {
        return this.getState();
    },
    getState: function () {
        return {

        };
    },
    render: function() {
        var steps = this.props.steps;
        var stepsDOM = [];
        steps.forEach(function(step, index) {
            step.id = step.id || step._id;
            stepsDOM.push(
                <Step key={step.id} index={index} step={step} settings={this.props.settings} onStepChange={this.onStepChange} moveStep={this.moveStep}/>
            );
        }.bind(this));
        return (
            <div className="step-list">
                {stepsDOM}
            </div>
        );
    },
    onStepChange: function(step, i) {
        var steps = this.props.steps;
        steps[i] = step;
        this.props.onStepsChange(steps);
    },
    moveStep: function(from, to) {
        var steps = this.props.steps;
        if(to >= 0 && to < steps.length)
            steps.move(from, to);
        this.props.onStepsChange(steps);
    }
});

module.exports = StepList;
