'use strict';
var React = require('react');

var EditableSelect = React.createClass({
    getInitialState: function () {
        return {
            editing: this.props.editing || false,
            value: this.props.defaultValue
        }
    },
    render: function() {
        var text = this.state.value ? "Yes" : "No";

        var label = this.props.label ? ( <label for={this.props.id}>{this.props.label}</label> ) : "";
        return (
            <div className="editable" onClick={this.toggle}>
                {label}
                <span>{text}</span>
            </div>
        );
    },
    toggle: function() {
        var value = !this.state.value;
        this.setState({
            value: value
        });
        if(this.props.onFieldChange)
            this.props.onFieldChange(this.props.id, value);
    }
});

module.exports = EditableSelect;
