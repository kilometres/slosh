'use strict';
var React = require('react');
var Path = require('./Path');
var updatePath = require('../actions/updatePath.js');
var updateTempPath = require('../actions/updateTempPath.js');
var clearTempPath = require('../actions/clearTempPath.js');
var setTempPathById = require('../actions/setTempPathById.js');
var FluxibleMixin = require('fluxible/lib/FluxibleMixin');
var PathStore = require('../stores/PathStore.js');

var EditPath = React.createClass({
    mixins: [FluxibleMixin],
    componentDidMount: function () {
        this.context.executeAction(setTempPathById, { id: this.props.params.id });
    },
    componentWillUnmount: function () {
        this.context.executeAction(clearTempPath);
    },
    render: function() {
        var path = this.props.tempPath;
        if(path)
            return (
                <div>
                    <Path path={path} settings={this.props.settings} onPathChange={this.onPathChange} />
                    <button onClick={this.save}>Update</button>
                </div>
            );
        else return (<span>Loading...</span>);
    },
    onPathChange: function(path) {
        this.context.executeAction(updateTempPath, { path: path, caller: this });
    },
    save: function() {
        this.context.executeAction(updatePath, { path: this.props.tempPath, caller: this });
    }
});

module.exports = EditPath;
