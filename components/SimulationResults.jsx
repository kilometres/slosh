'use strict';
var React = require('react');
var SimulatedRound = require('./SimulatedRound');

var SimulationResults = React.createClass({
    render: function() {
        var roundsDom = [];
        this.props.results.forEach(function(round, index) {
            roundsDom.push(
                <SimulatedRound key={index} round={round} roundNumber={index}/>
            );
        })
        return (
            <div className="simulation-results">
                {roundsDom}
            </div>
        );
    },

});

module.exports = SimulationResults;
