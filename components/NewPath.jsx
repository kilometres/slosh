'use strict';
var React = require('react');
var Path = require('./Path');
var createPath = require('../actions/createPath.js');
var FluxibleMixin = require('fluxible/lib/FluxibleMixin');
var Navigation = require('react-router').Navigation;

var NewPath = React.createClass({
    mixins: [FluxibleMixin, Navigation],
    getInitialState: function () {
        return this.getState();
    },
    getState: function () {
        return {
            path: {
                name: "New Path",
                description: "A new path",
                maximumUses: 0,
                steps: [{
                    instructions: "Drink.",
                    round: 1,
                    kind: this.props.settings.stepTypes[0],
                    weight: 10,
                    id: Date.now()
                }]
            }
        };
    },
    render: function() {
        return (
            <div className="view">
                <Path path={this.state.path} settings={this.props.settings} onPathChange={this.onPathChange}/>
                <button onClick={this.save}>Save</button>
            </div>
        );
    },
    onPathChange: function(path) {
        this.setState({
            path: path
        })
    },
    save: function() {
        this.context.executeAction(createPath, { path: this.state.path, caller: this });
    }
});

module.exports = NewPath;
