'use strict';
var React = require('react');

var SimulatedTurn = React.createClass({
    render: function() {
        var turn = this.props.turn;
        return (
            <div className="simulated-turn">
                <div className="turn-name">{turn.player.name}</div>
                <div className="turn-rule">{turn.place}) {turn.instructions}</div>
            </div>
        );
    },

});

module.exports = SimulatedTurn;
