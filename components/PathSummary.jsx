'use strict';
var React = require('react');
var Link = require('react-router').Link;

var PathSummary = React.createClass({
    getInitialState: function () {
        return this.getState();
    },
    getState: function () {
        return {

        };
    },
    render: function() {
        var path = this.props.path;
        return (
            <div className="card path-summary">
                <h2><Link to={'/path/' + path._id}>{path.number ? path.number + '. ' : ''}{path.name}</Link></h2>
                <p>{path.description}</p>
            </div>
        );
    }
});

module.exports = PathSummary;
