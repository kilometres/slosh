'use strict';
var React = require('react');
var EditableInput = require('./EditableInput');
var Step = require('./Step');
var StepList = require('./StepList');
var AddStep = require('./AddStep');

Array.prototype.move = function(from, to) {
    this.splice(to, 0, this.splice(from, 1)[0]);
};

var Path = React.createClass({
    render: function() {
        var path = this.props.path;
        if(path)
            return (
                <div className="path">
                    <EditableInput id="name" className="" type="text" defaultValue={path.name} valueRequired={true} label="name" onFieldChange={this.onFieldChange}/>
                    <EditableInput id="number" className="" type="number" defaultValue={path.number} valueRequired={true} label="no." onFieldChange={this.onFieldChange} />
                    <EditableInput id="description" className="" type="text" defaultValue={path.description} valueRequired={true} label="description" onFieldChange={this.onFieldChange}/>
                    <EditableInput id="maximumUses" className="" type="number" defaultValue={path.maximumUses} valueRequired={true} label="max uses" onFieldChange={this.onFieldChange} />
                    <br/>
                    <StepList steps={path.steps} settings={this.props.settings} onStepsChange={this.onStepsChange} />
                    <button onClick={this.addStep}>add step</button>
                </div>
            );
        else return (<span>Loading...</span>);
    },
    onFieldChange: function(field, value) {
      var path = this.props.path;
      path[field] = value;
      this.props.onPathChange(path);
    },
    onStepsChange: function(steps) {
        var path = this.props.path;
        path.steps = steps;
        this.props.onPathChange(path);
    },
    addStep: function() {
        var path = this.props.path;
        path.steps.push({
            kind: this.props.settings.stepTypes[0],
            instructions: "Drink.",
            round: 1,
            id: Date.now()
        });
        this.props.onPathChange(path);
    },
    deleteStep: function() {

    }
});

module.exports = Path;
