'use strict';
var React = require('react');
var Nav = require('./Nav.jsx');
var SettingsStore = require('../stores/SettingsStore');
var FluxibleMixin = require('fluxible/lib/FluxibleMixin');
var RouteHandler = require('react-router').RouteHandler;
var PathStore = require('../stores/PathStore.js');

var Application = React.createClass({
    mixins: [FluxibleMixin],
    statics: {
        storeListeners: [SettingsStore, PathStore]
    },
    getInitialState: function () {
        return this.getState();
    },
    getState: function () {
        var settingsStore = this.getStore(SettingsStore);
        var pathStore = this.getStore(PathStore);
        return {
          paths: pathStore.getPaths(),
          tempPath: pathStore.getTempPath(),
          settings: settingsStore.getSettings()
        };
    },
    onChange: function () {
        var state = this.getState();
        this.setState(state);
    },
    render: function () {
        return (
            <div>
                <Nav />
                <RouteHandler paths={this.state.paths} tempPath={this.state.tempPath} settings={this.state.settings} />
            </div>
        );
    }
});

module.exports = Application;
