'use strict';

/**
 * This leverages Express to create and run the http server.
 * A fluxible context is created and executes the navigateAction
 * based on the URL. Once completed, the store state is dehydrated
 * and the application is rendered via React.
 */

require('babel/register');

var express = require('express');
var restful = require('node-restful');
var mongoose = restful.mongoose;
var serialize = require('serialize-javascript');
var navigateAction = require('./actions/navigate');
var debug = require('debug')('slosheditor');
var React = require('react');
var app = require('./app');
var HtmlComponent = React.createFactory(require('./components/Html.jsx'));
var FluxibleComponent = require('fluxible/lib/FluxibleComponent');
var Router = require('react-router');
var util = require('util');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');

// Resources
var Path = require('./models/Path.js');
var Trash = require('./models/Trash.js');

var server = express();
server.use(bodyParser.urlencoded({'extended':'true'}));
server.use(bodyParser.json());
server.use(bodyParser.json({type:'application/vnd.api+json'}));
// //server.use(methodOverride());
server.use('/public', express.static(__dirname + '/build'));
//server.use('/public/fonts', express.static(__dirname + '/fonts'));
server.use('/bower', express.static(__dirname + '/bower_components'));

mongoose.connect('mongodb://test:pass@ds057862.mongolab.com:57862/slosh-quest2');

server.get('/api/paths', function(req, res) {
    Path.find(function(err, paths) {
        if(err) { console.warn(err); }
        // paths.forEach(function(path){
        //
        //     for(var i = 0; i < path.steps.length; i++){
        //         //path.steps[i].weight = 10;
        //
        //     }
        //     Path.update({ _id: path._id}, path, function(err) {
        //         if(err){
        //           console.log(err);
        //         }
        //     });
        // });
        res.json(paths);
    });
});

server.post('/api/paths', function(req, res) {
    var path = new Path(req.body);
    path.save(function (err, path) {
        if(err) { console.warn(err); }
        res.json(path);
    });
});

server.put('/api/paths/:id', function(req, res) {
    Path.update({ _id: req.params.id}, req.body, function(err, path) {
        if(err) { console.warn(err); }
        res.json(path);
    });
});

server.delete('/api/paths/:id', function(req, res) {
    Path.findOne({ _id: req.params.id }, function(err, path){
        path.remove(function(err) {
            if(err) {
                console.warn(err);
                res.send(false);
            } else {
                var trash = new Trash(path);
                trash.save(function (err, trash) {
                    if(err) {console.warn(err); }
                    console.log(trash);
                    res.send('Moved to trash.');
                });
            }
        });
    });
});

server.use(function (req, res, next) {
    var context = app.createContext();
    debug('Executing navigate action');
    Router.run(app.getComponent(), req.path, function (Handler, state) {
        context.executeAction(navigateAction, state, function () {
            debug('Exposing context state');
            var exposed = 'window.App=' + serialize(app.dehydrate(context)) + ';';
            debug('Rendering Application component into html');
            var Component = React.createFactory(Handler);
            var html = React.renderToStaticMarkup(HtmlComponent({
                state: exposed,
                markup: React.renderToString(
                    React.createElement(
                        FluxibleComponent,
                        { context: context.getComponentContext() },
                        Component()
                    )
                )
            }));

            debug('Sending markup');
            res.send(html);
        });
    });
    // context.getActionContext().executeAction(navigateAction, {
    //     url: req.url
    // }, function (err) {
    //     if (err) {
    //         if (err.status && err.status === 404) {
    //             next();
    //         } else {
    //             next(err);
    //         }
    //         return;
    //     }
    //
    //     debug('Exposing context state');
    //     var exposed = 'window.App=' + serialize(app.dehydrate(context)) + ';';
    //
    //     debug('Rendering Application component into html');
    //     var html = React.renderToStaticMarkup(htmlComponent({
    //         context: context.getComponentContext(),
    //         state: exposed,
    //         markup: React.renderToString(context.createElement())
    //     }));
    //
    //     debug('Sending markup');
    //     res.type('html');
    //     res.write('<!DOCTYPE html>' + html);
    //     res.end();
    // });
});

var port = process.env.PORT || 3000;
server.listen(port);
console.log('Listening on port ' + port);
