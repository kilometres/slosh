'use strict';
var request = require('request');

module.exports = function clearTempPath(context, payload, done) {
    //console.log(payload);
    context.dispatch('CLEAR_TEMP_PATH', payload.path);
    done();
};
