'use strict';
var request = require('request');
var config = require('../config.js');

module.exports = function createPath(context, payload, done) {
    context.dispatch('DELETING_PATH');
    var options = {
        method: 'delete',
        url: config.host + '/api/paths/' + payload.id,
        json: true
    };
    request(options, function(err, res) {
        if (err) {
            console.log(res.body);
            context.dispatch('DELETING_PATH_FAILED');
        } else {
            context.dispatch('DELETE_PATH', payload.id);
            payload.caller.transitionTo('dash');
        }
    }.bind(this));
    done();
};
