'use strict';
var request = require('request');
var config = require('../config.js');

module.exports = function createPath(context, payload, done) {
    context.dispatch('CREATING_PATH');
    var options = {
        method: 'post',
        url: config.host + '/api/paths',
        body: payload.path,
        json: true
    };
    request(options, function(err, res, path) {
        if (err) {
            context.dispatch('CREATING_PATH_FAILED');

        }
        context.dispatch('CREATE_PATH', path);
        payload.caller.transitionTo('path', {id: path._id});
    }.bind(this));
    done();
};
