'use strict';
var request = require('request');
var config = require('../config.js');


module.exports = function (context, payload, done) {
    console.log(payload);
    context.dispatch('UPDATING_PATH');
    var options = {
        method: 'put',
        url: config.host + '/api/paths/' + payload.path._id,
        body: payload.path,
        json: true
    };
    request(options, function(err, res, path) {
        if (err) {
            context.dispatch('UPDATING_PATH_FAILED');
        }
        context.dispatch('UPDATE_PATH', payload.path);
    }.bind(this));
    done();
};
