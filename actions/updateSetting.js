'use strict';
var request = require('request');

module.exports = function (context, payload, done) {
    //console.log(payload);
    switch(payload.field) {
        case 'rounds':
            context.dispatch('SET_ROUNDS', payload.value);
            break;
        case 'cycles':
            context.dispatch('SET_CYCLES', payload.value);
            break;
    }
    done();
};
