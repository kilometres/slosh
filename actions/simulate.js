'use strict';
var request = require('request');

module.exports = function simulate(context, payload, done) {
    console.log('Simulation action...');
    context.dispatch('INIT_GAME');
    for(var i = 0; i < 4; i++) {
        context.dispatch('ADD_PLAYER', {
            name: 'Player' + (i + 1).toString(),
        });
    }
    context.dispatch('START_GAME');
    context.dispatch('RUN_SIMULATION');
    done();
};
