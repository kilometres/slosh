'use strict';
var createStore = require('fluxible/addons').createStore;
var PathHistories = require('../classes/PathHistories');
var request = require('request');

var UndoStore = createStore({
    storeName: 'UndoStore',
    handlers: {
        'NEW_PATH_VERSION': 'newVersion',
    },
    initialize: function () {
        this.pathHistories = new PathHistories();
    },
    newVersion: function (path) {
        this.pathHistories.newVersion(path);
        this.emitChange();
        this.attemptUpdate(path.id);
    },
    attemptUndo: function (id) {
        this.pathHistories.undo(id);
        this.emitChange();
    },
    attemptRedo: function (id) {
        this.pathHistories.redo(id);
        this.emitChange();
    },
    getCurrentVersion: function (id) {
        return this.pathHistories.get(id).current();
    },
    dehydrate: function () {
        return {
            steps: this.steps
        };
    },
    rehydrate: function (state) {
        this.steps = state.steps;
    }
});

module.exports = UndoStore;
