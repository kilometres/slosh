'use strict';
var createStore = require('fluxible/addons').createStore;
var GameStore = require('./GameStore');
var request = require('request');

var SimulationStore = createStore({
    storeName: 'SimulationStore',
    handlers: {
        'RUN_SIMULATION': 'simulate',
    },
    initialize: function () {
        this.results = [];
    },
    simulate: function () {
        var gameStore = this.dispatcher.getStore(GameStore);
        var counter = 0;
        this.results = [];

        while(!gameStore.isGameOver() && counter < 2000) {
            var round = gameStore.getCurrentRound();
            console.log(round);
            this.results[round] = this.results[round] || [];
            var result = gameStore.getCurrentCard();
            result.instructions = result.instructions.replace(/%LP/, result.lastPlayer ? result.lastPlayer.name : 'unknown');
            result.player = gameStore.getCurrentPlayer();
            result.place = counter + 1;
            this.results[round].push(JSON.parse(JSON.stringify(result)));

            gameStore.nextTurn();
            counter += 1;
        }

        this.emitChange();
    },
    getResults: function () {
        return this.results;
    },
    dehydrate: function () {
        return {
            results: this.results
        };
    },
    rehydrate: function (state) {
        this.results = state.results;
    }
});

module.exports = SimulationStore;
