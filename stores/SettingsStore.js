'use strict';
var createStore = require('fluxible/addons').createStore;
var request = require('request');

var SettingsStore = createStore({
    storeName: 'SettingsStore',
    handlers: {
        'LOAD_SETTINGS': 'loadSettings',
        'SET_CYCLES': 'setCycles',
        'SET_ROUNDS': 'setRounds'
    },
    initialize: function () {
        this.settings = {
            maxRounds: 5,
            maxCycles: 1,
            stepTypes: [
                {
                    id: 0,
                    name: 'random pool'
                },
                {
                    id: 1,
                    name: 'series'
                },
                {
                    id: 2,
                    name: 'sequence'
                },
                {
                    id: 3,
                    name: 'group sequence'
                }
            ]
        };
        this.loadSettings();
    },
    setCycles: function(value) {
        if(value && value > 0) {
            this.cycles = value;
        }
    },
    setRounds: function(value) {
        if(value && value > 0) {
            this.rounds = value;
        }
    },
    loadSettings: function () {

    },
    getSettings: function () {
        return this.settings;
    },
    dehydrate: function () {
        return {
            settings: this.settings
        };
    },
    rehydrate: function (state) {
        this.settings = state.settings;
    }
});

module.exports = SettingsStore;
