'use strict';
var createStore = require('fluxible/addons').createStore;
var UndoStore = require('./UndoStore');
var request = require('request');
var config = require('../config.js');

var PathStore = createStore({
    storeName: 'PathStore',
    handlers: {
        'LOAD_PATHS': 'loadPaths',
        'CREATE_PATH': 'createPath',
        'UPDATE_PATH': 'updatePath',
        'DELETE_PATH': 'deletePath',
        'SET_TEMP_PATH_BY_ID': 'setTempPathById',
        'UPDATE_TEMP_PATH': 'updateTempPath',
        'CLEAR_TEMP_PATH': 'clearTempPath'
    },
    initialize: function () {
        this.paths = new Map();
        this.tempPath = undefined;
        this.tempPathId = 0;
        this.loadPaths();
    },
    loadPaths: function () {
        var undoStore = this.dispatcher.getStore(UndoStore);
        request(config.host + '/api/paths', function(err, res, paths) {
            paths = JSON.parse(paths);
            for(var i = 0; i < paths.length; i++){
                this.paths.set(paths[i]._id, paths[i]);
            }
            this.setTempPathById(this.tempPathId);
            undoStore.initialize(this.paths);
            this.emitChange();
        }.bind(this));
    },
    getPaths: function () {
        return this.paths;
    },
    createPath: function (path) {
        this.paths.set(path._id, path);
        this.emitChange();
    },
    updateTempPath: function (path) {
        this.tempPath = path;
        this.emitChange();
    },
    updatePath: function (path) {
        this.paths.set(path._id, path);
        this.emitChange();
    },
    deletePath: function (id) {
        this.paths.delete(id);
        this.emitChange();
    },
    setTempPathById: function (id) {
        this.tempPathId = id;
        var path = this.paths.get(this.tempPathId);
        if(path) {
            this.tempPath = JSON.parse(JSON.stringify(path));
        }
        this.emitChange();
    },
    clearTempPath: function () {
        this.tempPath = undefined;
        this.emitChange();
    },
    getTempPath: function() {
        return this.tempPath;
    },
    dehydrate: function () {
        return {
            paths: this.paths
        };
    },
    rehydrate: function (state) {
        this.paths = state.paths;
    }
});

module.exports = PathStore;
