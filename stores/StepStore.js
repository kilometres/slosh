'use strict';
var createStore = require('fluxible/addons').createStore;
var request = require('request');

var StepStore = createStore({
    storeName: 'StepStore',
    handlers: {
        'LOAD_STEPS': 'loadSteps',
    },
    initialize: function () {
        this.steps = 'Blah';
        this.loadSteps();
    },
    loadSteps: function () {
        // request('http://localhost:3000/steps', function(err, res, body) {
        //   err = res;
        //   this.steps = body;
        //   this.emitChange();
        // }.bind(this));
    },
    getSteps: function () {
        return this.steps;
    },
    dehydrate: function () {
        return {
            steps: this.steps
        };
    },
    rehydrate: function (state) {
        this.steps = state.steps;
    }
});

module.exports = StepStore;
