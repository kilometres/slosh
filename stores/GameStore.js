'use strict';
var createStore = require('fluxible/addons').createStore;
var DeckStore = require('./DeckStore');
var PathStore = require('./PathStore');
var SettingsStore = require('./SettingsStore');
var request = require('request');

var GameStore = createStore({
    storeName: 'GameStore',
    handlers: {
        'ADD_PLAYER': 'addPlayer',
        'INIT_GAME': 'initialize',
        'START_GAME': 'startGame',
        'NEXT_TURN': 'nextTurn'
    },
    initialize: function () {
        this.players = new Map();
        this.currentPlayerId = null;
        this.playerRefs = [];
        this.currentPlayerIndex = -1;
        this.card = undefined;
        this.startingRound = 1;
        this.maxRounds = 5;
        this.maxCycles = 1;
        this.round = 1;
        this.cycles = 0;
        this.currentRound = 1;
        this._isGameOver = false;
    },
    updateSettings: function () {
        var settingsStore = this.dispatcher.getStore(SettingsStore);
        var settings = settingsStore.getSettings();
        this.maxRounds = settings.maxRounds;
        this.maxCycles = settings.maxCycles;
    },
    addPlayer: function (player) {
        player.id = this.generateUUID();
        console.log(player.name + '  -  ' + player.id);
        this.players.set(player.id, player);
        this.emitChange();
    },
    generateUUID: function () {
        var d = new Date().getTime();
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = (d + Math.random()*16)%16 | 0;
            d = Math.floor(d/16);
            return (c=='x' ? r : (r&0x3|0x8)).toString(16);
        });
        return uuid;
    },
    startGame: function () {
        this.updateSettings();
        var paths = this.dispatcher.getStore(PathStore).getPaths();
        this.dispatcher.getStore(DeckStore).initDeck(paths, this.players);

        this.players.forEach(function(player) {
            this.playerRefs.push(player.id);
        }.bind(this));

        this.nextTurn();
    },
    nextTurn: function () {
        if(this.iteratePlayer()) {
            if(this.iterateCycle()) {
                if(this.iterateRound()) {
                    this.endGame();
                    return false;
                }
            }
        }
        this.card = this.drawCard();
        this.emitChange();
    },
    iteratePlayer: function () {
        var isNextCycle = this.currentPlayerIndex + 1 >= this.playerRefs.length;
        this.currentPlayerIndex = isNextCycle ? 0 : this.currentPlayerIndex + 1;
        return isNextCycle;
    },
    iterateCycle: function() {
        var isNextRound = this.cycles + 1 >= this.maxCycles;
        this.cycles = isNextRound ? 0 : this.cycles + 1;
        return isNextRound;
    },
    iterateRound: function() {
        this.currentRound += 1;
        if(this.currentRound > this.maxRounds) { // The game is over
            return true;
        }
        else { // Add new paths for this round
            var deckStore = this.dispatcher.getStore(DeckStore);
            deckStore.addPathsToDeck(deckStore.getPathsByRound(this.currentRound));
        }
    },
    getCurrentPlayer: function () {
        return this.players.get(this.playerRefs[this.currentPlayerIndex]);
    },
    getPlayerById: function (id) {
        return this.players.get(id);
    },
    drawCard: function () {
        return this.dispatcher.getStore(DeckStore).draw(this.getCurrentPlayer(), this.currentRound);
    },
    getCurrentCard: function () {
        return this.card;
    },
    getCurrentRound: function () {
        return this.currentRound;
    },
    isGameOver: function () {
        return this._isGameOver;
    },
    endGame: function () {
        this._isGameOver = true;
        this.emitChange();
    },
    dehydrate: function () {
        return {
            startingRound: this.startingRound,
            maxRounds: this.maxRounds,
            maxCycles: this.maxCycles,
            cycles: this.cycles,
            currentRound: this.currentRound,
            _isGameOver: this._isGameOver
        };
    },
    rehydrate: function (state) {
        this.startingRound = state.startingRound;
        this.maxRounds = state.maxRounds;
        this.maxCycles = state.maxCycles;
        this.cycles = state.cycles;
        this.currentRound = state.currentRound;
        this._isGameOver = state._isGameOver;
    }
});

module.exports = GameStore;
