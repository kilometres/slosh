'use strict';
var createStore = require('fluxible/addons').createStore;
var request = require('request');
var rwc = require('random-weighted-choice');

function a(id, message, ref) {
    if(id == '55c80db6bc5f24434861a4cc') {
        console.log(message);
        ref.cardCopies(id);
    }
}

var DeckStore = createStore({
    storeName: 'DeckStore',
    handlers: {
        'INIT_DECK': 'initDeck',
    },
    initialize: function () {
        this.settings = {

        };
        this.paths = new Map();
        this.playerDecks = new Map();
        this.deck = [];
        this.counter = 0;
    },
    initDeck: function (paths, players) {
        // Group the paths by round
        // Add several fields to help with drawing, iterating and tracking paths
        paths.forEach(function(path) {
            path.currentStepIndex = 0;
            path.id = path._id;
            path.plays = 0;
            path.history = [];
            this.paths.set(path.id, path);
            this.addPathsToDeck(this.getPathsByRound(1));
        }.bind(this));

        // Create empty draw piles for the players
        // One is a queue, which takes precedence and is drawn in order
        // The other is a random draw pile
        players.forEach( function(player) {
            this.playerDecks.set(player.id, {
                queue: [],
                random: [],
                name: player.name
            });
        }.bind(this));
        this.emitChange();
    },
    draw: function (player, currentRound) {
        this.counter += 1;
        var pathId, drawnStep, origin;
        var playerId = player.id;
        var playerDeck = this.playerDecks.get(playerId);
        // Draw from the player's card queue
        if(playerDeck.queue.length > 0) {
            pathId = playerDeck.queue[0].id;
            origin = 'queue';
        }
        // Draw from the player's random deck
        else if (playerDeck.random.length > 0 && Math.random() > 0.66) {
            pathId = rwc(playerDeck.random);
            origin = 'random';
        }
        // Draw from the group pile
        else {
            pathId = rwc(this.deck);
            origin = 'main';
        }
        var path = this.paths.get(pathId);


        // House keeping //
        ///////////////////

        // Reset the path if the path isn't played out or if it's infinite
        var nextStepIndex = path.currentStepIndex + 1;
        if(nextStepIndex >= path.steps.length && (path.maximumUses > path.plays || path.maximumUses === 0)){
            path.plays += 1;
            nextStepIndex = 0;
        } else if (path.maximumUses <= path.plays && path.maximumUses > 0) { //Otherwise, discard the path
            this.removePath(path.id);
        }

        // Move path to appropriate deck or queue
        var nextStep = path.steps[nextStepIndex];
        var pathRef;

        var destination;
        if(nextStep.round > currentRound) {
            this.removePath(path.id);
            destination = 'removed';
        }
        else {
            destination = 'attempted ' + nextStep.kind.name;
            switch(nextStep.kind.name) {
                case 'random pool':
                    pathRef = this.removePath(playerId, path.id);
                    if(pathRef) { pathRef.weight = nextStep.weight; this.deck.push(pathRef); destination = 'random pool'; }
                    break;
                case 'series':
                    pathRef = this.removePath(path.id);
                    if(pathRef) { pathRef.weight = nextStep.weight; playerDeck.queue.push(pathRef); destination = 'series';}
                    this.playerDecks.set(playerId, playerDeck);
                    break;
                case 'sequence':
                    pathRef = this.removePath(path.id);
                    if(pathRef) { pathRef.weight = nextStep.weight; playerDeck.random.push(pathRef); destination = 'sequence';}
                    this.playerDecks.set(playerId, playerDeck);
                    break;
                case 'group sequence':
                    pathRef = this.removePath(playerId, path.id);
                    if(pathRef) { pathRef.weight = nextStep.weight; this.deck.push(pathRef); destination = 'group sequence';}
                    break;
            }
        }

        a(path.id, this.counter + ')  ' + player.name + ' draws from ' + origin + ' and places into ' + playerId.slice(0, 5) + ' ' + destination + '  --  Step? ' + nextStep.kind.name, this);

        path.history.splice(0, 0, player);
        drawnStep = path.steps[path.currentStepIndex];
        drawnStep.lastPlayer = path.history[1];

        // Put the path changes in the deck array
        path.currentStepIndex = nextStepIndex;
        this.updatePath(path.id, JSON.parse(JSON.stringify(path)));

        return drawnStep;
    },
    removePath: function (id) {
        var path = this.removePathFromDeck(id);
        this.playerDecks.forEach(function (deck, playerId) {
            if(!path) {
                path = this.removePathFromPlayerDeck(playerId, id);
            }
        }.bind(this));
        return path;
    },
    removePathFromDeck: function (id) {
        for(var i = 0; i < this.deck.length; i++){
            if(this.deck[i].id == id) {
                return this.deck.splice(i, 1)[0];
            }
        }
        return undefined;
    },
    removePathFromPlayerDeck: function (playerId, id) {
        var deck = this.playerDecks.get(playerId);
        var path = false;
        for(var i = 0; i < deck.queue.length; i++) {
            if(deck.queue[i].id == id) {
                path = deck.queue.splice(i, 1)[0];
                break;
            }
        }
        for(i = 0; i < deck.random.length; i++) {
          if(deck.random[i].id == id) {
              path = deck.random.splice(i, 1)[0];
              break;
          }
        }

        this.playerDecks.set(playerId, deck);
        return path;
    },
    updatePath: function (id, path) {
        this.paths.set(id, path);
    },
    getPathsByRound: function (round) {
        var paths = new Map();
        this.paths.forEach(function(path) {
            if(path.steps[path.currentStepIndex].round === round) {
                paths.set(path.id, path);
            }
        });
        return paths;
    },
    addPathsToDeck: function(paths) {
        paths.forEach(function(path) {
            var pathRef = {
                weight: path.steps[path.currentStepIndex].weight,
                id: path.id
            };
            var playerId = path.history[0] ? path.history[0].id : 'noid';
            var playerDeck = this.playerDecks.get(playerId);

            switch(path.steps[path.currentStepIndex].kind.name) {
                case 'random pool':
                    if(!this.hasPath(this.deck, pathRef.id)) {
                        this.deck.push(pathRef);
                    }
                    break;
                case 'series':
                    if(!this.hasPath(playerDeck.queue, pathRef.id)) {
                        playerDeck.queue.push(pathRef);
                        this.playerDecks.set(playerId, playerDeck);
                    }
                    break;
                case 'sequence':
                    if(!this.hasPath(playerDeck.random, pathRef.id)) {
                        playerDeck.random.push(pathRef);
                        this.playerDecks.set(playerId, playerDeck);
                    }
                    break;
                case 'group sequence':
                    if(!this.hasPath(this.deck, pathRef.id)) {
                        this.deck.push(pathRef);
                    }
                    break;
            }
        }.bind(this));
    },
    hasPath: function(deck, id) {
        var has = 0;
        deck.forEach(function(path) {
            if(path.id == id) {
                has += 1;
            }
        });
        return has;
    },
    cardCopies: function(id) {
        var playerNums = 0;
        this.playerDecks.forEach(function(deck) {
            playerNums +=  this.hasPath(deck.random, id);
            playerNums +=  this.hasPath(deck.queue, id);
        }.bind(this));
        console.log(id + ' total: ' + (playerNums + this.hasPath(this.deck, id)));
    },
    dehydrate: function () {
        return {
            settings: this.settings,
            paths: this.paths,
            playerDecks: this.playerDecks,
            deck: this.deck
        };
    },
    rehydrate: function (state) {
        this.settings = state.settings;
        this.paths = state.paths;
        this.playerDecks = state.playerDecks;
        this.deck = state.deck;
    }
});

module.exports = DeckStore;
